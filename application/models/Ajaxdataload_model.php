<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxdataload_model extends CI_Model
{
	function __construct() {
        parent::__construct(); 
    }

	/*----------------------------- BOOKS -------------------------------*/

	function all_books_count()
	{
		$query = $this->db->get('book');
		return $query->num_rows();
	}

	function all_books($limit, $start, $col, $dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('book');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function book_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->like('name', $search)
                ->or_like('author', $search)
                ->or_like('book_id', $search)
                ->or_like('price', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('book');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function book_search_count($search)
    {
        $query = $this
                ->db
                ->like('name', $search)
                ->or_like('book_id', $search)
                ->or_like('author', $search)
                ->or_like('price', $search)
                ->get('book');
    
        return $query->num_rows();
    }

	/*----------------------------- BOOKS -------------------------------*/


    /*----------------------------- PRODI -------------------------------*/


    function getMaxSemester($class_id)
    {   
       $query = $this->db
                ->select("p.semester")
                ->where("c.class_id",$class_id)
                ->join("prodi p","p.prodi_id = c.prodi_id")
                ->get('class c');
        
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return null;
        
    } /*----------------------------- TEACHERS -------------------------------*/

    function all_teachers_count()
    {
        $query = $this->db->get('teacher');
        return $query->num_rows();
    }

    function all_teachers($limit, $start, $col, $dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('teacher');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function teacher_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->like('teacher_id', $search)
                ->or_like('name', $search)
                ->or_like('email', $search)
                ->or_like('phone', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('teacher');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function teacher_search_count($search)
    {
        $query = $this
                ->db
                ->like('teacher_id', $search)
                ->or_like('name', $search)
                ->or_like('email', $search)
                ->or_like('phone', $search)
                ->get('teacher');
    
        return $query->num_rows();
    }

    /*----------------------------- TEACHERS -------------------------------*/


    /*----------------------------- STUDENT -------------------------------*/
    function get_student($id)
    {
        $query = $this
                ->db
                ->select("*")
                ->where('student_id', $id)
                ->get('student');
        
        if($query->num_rows() > 0){
            $query = $query->result();
            return $query;
        }
        else{
            return null;
        }
        
    }


    /*----------------------------- PARENTS -------------------------------*/

    function all_parents_count()
    {
        $query = $this->db->get('parent');
        return $query->num_rows();
    }

    function all_parents($limit, $start, $col, $dir)
    {   
        $query2 = $this
                ->db
                ->select("p.*,GROUP_CONCAT(s.name ORDER By s.name) names,GROUP_CONCAT(s.student_id ORDER By s.student_id) idAll")
                 ->join('student s','s.parent_id=p.parent_id', 'left')
                ->limit($limit,$start)
                ->group_by('p.parent_id')
                ->order_by('p.parent_id')
                ->get_compiled_select('parent p');
         
          $query = $this->db->query($query2);             
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function parent_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->select("p.*,GROUP_CONCAT(s.name ORDER By s.name) names,GROUP_CONCAT(s.student_id ORDER By s.student_id) idAll")
                ->like('parent_id', $search)
                ->or_like('name', $search)
                ->or_like('email', $search)
                ->or_like('phone', $search)
                ->or_like('profession', $search)
                ->join('student s','s.parent_id=p.parent_id')
                ->limit($limit, $start)
                ->group_by('s.parent_id')
                 ->order_by('p.'.$col,$dir)
                ->get('parent p');
        
        if($query->num_rows() > 0){
            $query = $query->result();
            //echo $this->db->last_query();die;
            return $query;
        }
        else{
            return null;
        }
        
    }

    function parent_search_count($search)
    {
        $query = $this
                ->db
                ->like('parent_id', $search)
                ->or_like('name', $search)
                ->or_like('email', $search)
                ->or_like('phone', $search)
                ->or_like('profession', $search)
                ->get('parent');
    
        return $query->num_rows();
    }

    /*----------------------------- PARENTS -------------------------------*/
    /*----------------------------- STUDENT -------------------------------*/
    function all_students2($q)
    {   
       $query = $this->db
                ->select("s.student_id id, s.name text")
                ->like('s.name', $q,'both')
                ->order_by('s.student_id')
                ->get('student s');
         $query->num_rows();
         echo $this->db->last_query();die;
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }  


    function all_students($q)
    {   
      $query = $this->db->select('student_id id, name text')
                ->like('name', $q,'both')
                ->get("student");
            $res= $query->result();
            return $res;
        
    }

    function all_students_attn($q,$class_id,$section_id)
    {   
      $query = $this->db->select('s.student_id id, s.name name_child, c.name_numeric,sc.name name_section')
                ->join("student s","e.student_id = s.student_id")                
                ->join("class c","c.class_id = e.class_id")
                ->join("section sc","sc.section_id = e.section_id")
                ->where("e.class_id !=",$class_id)
                ->or_where("e.section_id !=",$section_id)
                ->like('s.name', $q,'both')
                ->get("enroll e");
            $res= $query;
            //echo $this->db->last_query();
            return $res;
            //

    }

    function all_asal_sekolah($q)
    {   
      $query = $this->db->select('id, name')
                ->like('name', $q,'both')
                ->get("asal_sekolah");
            $res= $query;
            //echo $this->db->last_query();
            return $res;
            //

    }   

    function all_referensi_siswa($q)
    {   
      $query = $this->db->select('id, name')
                ->like('name', $q,'both')
                ->get("referensi_siswa");
            $res= $query;
            //echo $this->db->last_query();
            return $res;
            //

    }

    /*----------------------------- STUDENT -------------------------------*/

    /*----------------------------- EXPENSES -------------------------------*/

    function all_expenses_count()
    {
        $array = array('payment_type' => 'expense', 'year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->get('payment');
        return $query->num_rows();
    }

    function all_expenses($limit, $start, $col, $dir)
    {
        $array = array('payment_type' => 'expense', 'year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('payment');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function expense_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->like('payment_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('payment');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function expense_search_count($search)
    {
        $query = $this
                ->db
                ->like('payment_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->get('payment');
    
        return $query->num_rows();
    }

    /*----------------------------- EXPENSES -------------------------------*/


    /*----------------------------- INVOICES -------------------------------*/

    function all_invoices_count()
    {
        $array = array('year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->get('invoice');
        return $query->num_rows();
    }

    function all_invoices($limit, $start, $col, $dir)
    {
        $array = array('year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('invoice');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function invoice_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->like('invoice_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->or_like('status', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('invoice');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function invoice_search_count($search)
    {
        $query = $this
                ->db
                ->like('invoice_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->or_like('status', $search)
                ->get('invoice');
    
        return $query->num_rows();
    }

    /*----------------------------- INVOICES -------------------------------*/


    /*----------------------------- PAYMENTS -------------------------------*/

    function all_payments_count()
    {
        $array = array('payment_type' => 'income', 'year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->get('payment');
        return $query->num_rows();
    }

    function all_payments($limit, $start, $col, $dir)
    {
        $array = array('payment_type' => 'income', 'year' => get_settings('running_year'));
        $query = $this
                ->db
                ->where($array)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('payment');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
        
    }

    function payment_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
                ->db
                ->like('payment_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->limit($limit, $start)
                ->order_by($col, $dir)
                ->get('payment');
        
        if($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function payment_search_count($search)
    {
        $query = $this
                ->db
                ->like('payment_id', $search)
                ->or_like('title', $search)
                ->or_like('amount', $search)
                ->get('payment');
    
        return $query->num_rows();
    }

    /*----------------------------- PAYMENTS -------------------------------*/
}