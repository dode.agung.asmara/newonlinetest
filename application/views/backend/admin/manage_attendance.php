<hr />

<?php echo form_open(site_url('admin/attendance_selector/'));?>
<div class="row">

	<div class="col-md-6">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('class');?></label>
			<select name="class_id" class="form-control selectboxit" onchange="select_section(this.value)" id = "class_selection">
				<option value=""><?php echo get_phrase('select_class');?></option>
				<?php
					$classes = $this->db->get('class')->result_array();
					foreach($classes as $row):
                                            
				?>
                                
				<option value="<?php echo $row['class_id'];?>"
					><?php echo $row['name'];?></option>
                                
				<?php endforeach;?>
			</select>
		</div>
	</div>

	
    <div id="section_holder">
	<div class="col-md-6">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
			<select class="form-control selectboxit" name="section_id" onchange="select_routine($('#timestamp').val());">
                            <option value=""><?php echo get_phrase('select_class_first') ?></option>
				
			</select>
		</div>
	</div>
    </div>
	
    <div class="col-md-6">
		<div class="form-group">
		<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
			<input type="text" class="form-control datepicker" name="timestamp" id="timestamp" data-format="dd-mm-yyyy"
				value="<?php echo date("d-m-Y");?>" onchange="select_routine(this.value)"/>
		</div>
	</div>   
	
	    <div id="routine_holder">
    <div class="col-md-6">

        <div class="form-group">
            <label class="control-label" style="margin-bottom: 5px;">Mata Kuliah</label>
            <select name="routine_id" id="routine_id" class="form-control selectboxit">
            	 <option value=""><?php echo get_phrase('jadwal_first') ?></option>
                <?php
                $routine = $this->db
                            ->select("cr.*,s.name subjectname")
                            ->join("subject s", "s.subject_id = cr.subject_id")  
                            ->get_where('class_routine cr',array(
                                'cr.class_id' => $class_id,
                                'cr.section_id' => $section_id,
                                'day' => strtolower(date('l',$timestamp))))
                            ->result_array();
                            echo $this->db->last_query();
                foreach ($routine as $row):
                    ?>
                    <option value="<?php echo $row['class_routine_id']; ?>">
                            <?php echo $row['subjectname']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

    </div>
</div>
	<input type="hidden" name="year" value="<?php echo $running_year;?>">

	<div class="col-md-3" style="margin-top: 20px;">
		<button type="submit" id = "submit" class="btn btn-info"><?php echo get_phrase('manage_attendance');?></button>
	</div>

</div>
<?php echo form_close();?>

<script type="text/javascript">
var class_selection = "";
jQuery(document).ready(function($) {
	$('#submit').attr('disabled', 'disabled');
});

    function select_routine(date) {
        if (date !== '') {
        $.ajax({
            url: '<?php echo site_url('admin/get_routine/'); ?>' + $('#class_selection').children("option:selected").val() + "/" + $('#section_id').children("option:selected").val() + "/"+date,
            success:function (response)
            {
                jQuery('#routine_holder').html(response);
            }
        });
    }
    }
function select_section(class_id) {
	if(class_id !== ''){
		$.ajax({
			url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
			success:function (response)
			{

			jQuery('#section_holder').html(response);
			select_semester(class_id);
			select_routine($("#timestamp").val());
			}
		});
	}
}

function select_semester(class_id) {
	if(class_id !== ''){
		$.ajax({
			url: '<?php echo site_url('admin/get_semester/'); ?>' + class_id,
			success:function (response)
			{

			jQuery('#semester_holder').html(response);
			select_routine($("#timestamp").val());
			}
		});
	}
}

function check_validation(){
	if(class_selection !== ''){
		$('#submit').removeAttr('disabled')
	}
	else{
		$('#submit').attr('disabled', 'disabled');
	}
}

$('#class_selection').change(function(){
	class_selection = $('#class_selection').val();
	check_validation();
});
</script>